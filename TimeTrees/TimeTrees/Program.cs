﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using Newtonsoft.Json;

namespace TimeTrees
{
    public class  Person
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public DateTime BirthDate { get; set; }
        public DateTime? DeathDate { get; set; }
    }

    public class TimelineEvent
    {
        public DateTime DateEvent { get; set; }
        public string Description { get; set; }
    }

    class Program
    {
        private const int personIdIndex = 0;
        private const int personNameIndex = 1;
        private const int personBirthDateIndex = 2;
        private const int personDeathDateIndex = 3;

        private const int timelineDateIndex = 0;
        private const int timelineDescriptionIndex = 1;

        static string[][] ReadData(string path)
        {
            string[] data = File.ReadAllLines(path);
            string[][] splitData = new string[data.Length][];
            for (int i = 0; i < data.Length; i++)
            {
                var line = data[i];
                string[] parts = line.Split(";");
                splitData[i] = parts;
            }
            return splitData;
        }

        static Person[] ReadPeople(string path)
        {
            string[][] data = ReadData(path);
            Person[] people = new Person[data.Length];
            for (int i = 0; i < data.Length; i++)
            {
                var elements = data[i];
                Person person = new Person();
                person.Id = int.Parse(elements[personIdIndex]);
                person.Name = elements[personNameIndex];
                person.BirthDate = GetData(elements[personBirthDateIndex]);
                if (elements.Length == 4)
                    person.DeathDate = GetData(elements[personDeathDateIndex]);

                people[i] = person;
            }
            return people;
        }

        static TimelineEvent[] ReadEvents(string path)
        {
            string[][] data = ReadData(path);
            TimelineEvent[] events = new TimelineEvent[data.Length];
            for (int i = 0; i < data.Length; i++)
            {
                var elements = data[i];
                TimelineEvent timeline = new TimelineEvent();

                timeline.DateEvent = GetData(elements[timelineDateIndex]);
                timeline.Description = elements[timelineDescriptionIndex];

                events[i] = timeline;
            }
            return events;
        }

        static DateTime GetData(string myData)
        {
            DateTime data;
            if (myData.Length == 4)
            {
                data = DateTime.ParseExact(myData, "yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None);
                return data;
            }
            else if (myData.Length == 7)
            {
                data = DateTime.ParseExact(myData, "yyyy-MM", CultureInfo.InvariantCulture, DateTimeStyles.None);
                return data;
            }
            else
            {
                data = DateTime.ParseExact(myData, "yyyy-MM-dd", CultureInfo.InvariantCulture, DateTimeStyles.None);
                return data;
            }
        }

        static DateTime EarliestEvent(TimelineEvent[] timelineData)
        {
            DateTime earliestEvent = DateTime.MaxValue;
            foreach (var timeEvents in timelineData)
            {
                DateTime date = timeEvents.DateEvent;
                if (earliestEvent > date)
                    earliestEvent = date;
            }
            return earliestEvent;
        }

        static DateTime LatestEvent(TimelineEvent[] timelineData)
        {
            DateTime latestEvent = DateTime.MinValue;
            foreach (var timeEvents in timelineData)
            {
                DateTime date = timeEvents.DateEvent;
                if (latestEvent < date)
                    latestEvent = date;
            }
            return latestEvent;
        }

        static (int, int, int) DifMinAndMaxDate(TimelineEvent[] timeline)
        {
            DateTime minDate = EarliestEvent(timeline);
            DateTime maxDate = LatestEvent(timeline);
            return (maxDate.Year - minDate.Year,
                maxDate.Month - minDate.Month,
                maxDate.Day - minDate.Day);
        }

        static int GetYearsOld(Person DateOneHuman)
        {
            DateTime dateBirth = DateOneHuman.BirthDate;
            DateTime dateDeath;
            if (DateOneHuman.DeathDate == null)
                dateDeath = DateTime.Now;
            else
                dateDeath = (DateTime)DateOneHuman.DeathDate;
            int yearsOld = (dateDeath.Date - dateBirth.Date).Days / 365;
            return yearsOld;
        }

        static List<Person> ReadPeopleFromJson(string path)
        {
            string jsonPersonString = File.ReadAllText(path);
            return JsonConvert.DeserializeObject<List<Person>>(jsonPersonString.Replace(';', ' '));
        }
        
        static void PeopleIsJson(string path, List<Person> people)
        {
            string json = JsonConvert.SerializeObject(people);
            File.WriteAllText(path, json);
        }

        static void Main()
        {
            string timelineFile = Path.Combine(Environment.CurrentDirectory, "timeline.csv");
            string peopleFile = Path.Combine(Environment.CurrentDirectory, "people.csv");

            string timelineFileJson = Path.Combine(Environment.CurrentDirectory, "timeline.json");
            string peopleFileJson = Path.Combine(Environment.CurrentDirectory, "people.json");

            TimelineEvent[] timelineEvents = ReadEvents(timelineFile);
            Person[] people = ReadPeople(peopleFile);


            (int years, int months, int days) = DifMinAndMaxDate(timelineEvents);
            Console.WriteLine($"Между самым ранним и самым поздним событием прошло: {years} лет {months} месяцев и {days} дней");
            Console.Write("Эти ребята родились в високосный год и им не больше 21 года: ");
            foreach (var human in people)
            {
                if (DateTime.IsLeapYear(human.BirthDate.Year) && GetYearsOld(human) <= 20)
                {
                    Console.Write($"{human.Name} ");
                }
            }

            //PeopleIsJson(peopleFileJson, ReadPeopleFromJson(peopleFile));
        }
    }
}
